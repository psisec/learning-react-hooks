import React, { useState } from "react";
import axios from "axios";

const todo = props => {
  const [todoName, setTodoName] = useState("");
  const [todoList, setTodoList] = useState([]);
  //   const [todoState, setTodoState] = useState({ userInput: "", todoList: [] });

  const inputChangeHandler = event => {
    setTodoName(event.target.value);
    //     setTodoState({
    //       userInput: event.target.value,
    //       todoList: todoState.todoList
    //     });
    //     console.log(event.target.value, "event");
    // console.log(todoName, "TODONAME");
  };

  const todoAddHandler = () => {
    // setTodoState({
    //   userInput: todoState.userInput,
    //   todoList: todoState.todoList.concat(todoState.userInput)
    // });
    setTodoList(todoList.concat(todoName));
    // console.log(setTodoList, "SETTODOLIST");
    // console.log(todoList, "TODOLIST");
    console.log(todoName, "TODONAME");
    // console.log(setTodoName, "SETTODONAME");
    // console.log(todo, "todo");
    axios
      .post("https://reacthooks-f64a8.firebaseio.com/todos.json", {
        name: todoName
      })
      .then(res => {
        console.log(res, "result");
      })
      .catch(err => {
        console.log(err, "error");
      });
  };

  return (
    <React.Fragment>
      <input
        type="text"
        placeholder="Todo"
        onChange={inputChangeHandler}
        value={todoName}
      />
      <button type="button" onClick={todoAddHandler}>
        Add
      </button>
      <ul>
        {todoList.map(todo => (
          <li key={todo}>{todo}</li>
        ))}
      </ul>
    </React.Fragment>
  );
};

export default todo;
